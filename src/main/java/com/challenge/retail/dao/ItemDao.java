package com.challenge.retail.dao;

import com.challenge.retail.model.item.ItemType;

/**
 * Data access object
 * @author alexiakl
 *
 */
public class ItemDao {

	/**
	 * Get if an item is discountable or not
	 * @param itemType
	 * @return boolean
	 */
	public static boolean getIsDiscountable(ItemType itemType) {
		boolean isDiscountable = false;
		switch (itemType) {
		case GROCERIES:
			isDiscountable = false;
			break;

		default:
			isDiscountable = true;
			break;
		}
		
		return isDiscountable;
	}

}
