package com.challenge.retail.dao;

/**
 * Data access object
 * @author alexiakl
 *
 */
public class BillDao {
	
	/**
	 * For each multiplier of DiscountEvery reached, the bill is discounted DiscountEveryAmount
	 * int count = (int)(totalBillValue / DiscountEvery);
	 * double discount = count * DiscountEveryAmount 
	 * @return double
	 */
	public static double getDiscountEvery() {
		return 100;
	}
	
	/**
	 * Get Discount Every Amount
	 * Check getDiscountEvery method
	 * @return double
	 */
	public static double getDiscountEveryAmount() {
		return 5;
	}
}
