# Retail Demo

## Setup

To setup the project, clone the following repository:

```bash
git clone https://bitbucket.org/euriskoteam/retail-website-java-backend-alexi
```
Then execute the mvn install command in the root directory of the cloned project:

```bash
mvn install
```

## Unit Tests

To run all the unit tests:

```bash
mvn test
```
